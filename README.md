# Simplon Labège 5 / 2022~2023

- [A. démo carousel HTML/css/JS](./demo-carousel)
- [B. démo PHP-MySQL via Docker](./docker-compose-lamp)
- [C. démo PHP-Sessions](./demo-session)
- [D. démo PHP-Objets (Pokémon)](./demo-objet-pokemon)
- [E. démo Bootstrap CSS](./demo-bootstrapcss)
- [F. démo compilation Bootstrap](./dem7-bootstrap-compile)
- [G. démo Intro à Express JS](./demo-express)
- [H. démo Laravel](./demo-Laravel)
- [I. démo Typescript, ParcelJS et Promises](./demo-ts-parcel-promise)
- [J. démo Svelte](./demo-svelte)
- [K. démo Symfony](./demo-symfony)
- [L. projet Immo3](./immmo3)

