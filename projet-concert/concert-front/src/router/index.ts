import { createRouter, createWebHistory } from "vue-router";
import HomeView from "../views/HomeView.vue";


const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: "/",
      name: "home",
      component: HomeView,
    },
    {
      path: "/festival/:id",
      name: "festival",
      component: () => import("../views/FestivalView.vue"),
    },
    {
      path: "/reservation/:id",
      name: "reservation",
      component: () => import("../views/ReservationView.vue"),
    },
    {
      path: "/confirmation/:id/:firstname?/:lastname?/:email?",
      name: "confirmation",
      component: () => import("../views/ConfirmationView.vue"),
    },
    {
      path: "/thanks/:msg?",
      name: "thanks",
      component: () => import("../views/ThanksView.vue"),
    },
  ],
});

export default router;
