

export interface Festival {
  id: number
  attributes: {
    title: string
    from: string
    to: string
    place: string
  }
}

export const emptyFestival:Festival = {
  id:0,
  attributes: {
    title:'',
    from:'',
    to:'',
    place:''
  }
}

export interface Festivals {
  data: Festival[]
}

export interface Artist {
  id: number
  attributes: {
    name: string
    description: string
    picture: {
      data:{
        attributes:{
          url:string
        }
      }[]
    }
    events: string
  }
}

export interface Artists {
  data: Artist[]
}
