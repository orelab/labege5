
# Notes

- Utilisez l'appli [Draw.io](https://app.diagrams.net/) pour ouvrir le schéma explicatif du code !
- Regardez les [commits](https://gitlab.com/orelab/labege5/-/commits/main)
- Regardez les [commentaires dans le code](https://gitlab.com/orelab/labege5/-/blob/main/demo-carousel/app.js) Javascript