const carousel = [
  {
    id: 1,
    text: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus maximus lacinia ex in lacinia. Pellentesque lobortis leo eu lorem dapibus, vitae ultrices libero maximus. Quisque mi libero, blandit id commodo sed, pulvinar ac mauris. Aliquam luctus sapien mi, quis dapibus ex luctus vel. Proin vitae interdum risus. Aliquam erat est, elementum eget viverra nec, tincidunt in mi. Nam sit amet lacus at libero ornare sagittis.",
    image: "david-french-N0e6W2WDa5U-unsplash.jpg",
    credit: 'Photo de <a href="https://unsplash.com/@avidrench?utm_source=unsplash&utm_medium=referral&utm_content=creditCopyText">David French</a> sur <a href="https://unsplash.com/fr/s/photos/photo-identit%C3%A9?utm_source=unsplash&utm_medium=referral&utm_content=creditCopyText">Unsplash</a>'
  },
  {
    id: 2,
    text: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla vehicula nibh ac rutrum vehicula. Aliquam dignissim malesuada velit, sed suscipit odio lobortis et. Vestibulum porta libero urna, sed gravida metus mattis vulputate. Praesent pretium tellus ac libero efficitur interdum. Quisque ac mi rutrum, dictum lorem vestibulum, hendrerit eros. Morbi rutrum maximus egestas. Duis rhoncus sagittis accumsan.",
    image: "joecalih-WyBizVgCrDY-unsplash.jpg",
    credit: 'Photo de <a href="https://unsplash.com/@karsten116?utm_source=unsplash&utm_medium=referral&utm_content=creditCopyText">Karsten Winegeart</a> sur <a href="https://unsplash.com/fr/s/photos/photo-identit%C3%A9?utm_source=unsplash&utm_medium=referral&utm_content=creditCopyText">Unsplash</a>'
  },
  {
    id: 3,
    text: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla tempor at dolor non viverra. Curabitur porta at mi nec venenatis. Phasellus sagittis consequat dolor, et mattis ligula eleifend a. Etiam in nisi at arcu interdum sagittis. Etiam mauris metus, suscipit vestibulum nisi id, eleifend tincidunt elit. Phasellus non mi mauris. Mauris id dignissim ligula, eu viverra lectus. Proin rutrum tellus nec iaculis vehicula. Aenean semper magna at orci dignissim pulvinar.",
    image: "karsten-winegeart-hS8kY12h3is-unsplash.jpg",
    credit: 'Photo de <a href="https://unsplash.com/@joecalih?utm_source=unsplash&utm_medium=referral&utm_content=creditCopyText">Joecalih</a> sur <a href="https://unsplash.com/fr/s/photos/photo-identit%C3%A9?utm_source=unsplash&utm_medium=referral&utm_content=creditCopyText">Unsplash</a>'
  },
  {
    id: 4,
    text: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc sapien augue, tempus et euismod sed, faucibus at mauris. Sed nisl nisl, tristique et pellentesque vehicula, rhoncus ornare nunc. Morbi rutrum sem nisl, et ornare ex condimentum at. Praesent et finibus erat. Nunc gravida tincidunt dolor a eleifend.",
    image: "mahdi-bafande-hr7eefjrekI-unsplash.jpg",
    credit: 'Photo de <a href="https://unsplash.com/@mahdibafande?utm_source=unsplash&utm_medium=referral&utm_content=creditCopyText">Mahdi Bafande</a> sur <a href="https://unsplash.com/fr/s/photos/photo-identit%C3%A9?utm_source=unsplash&utm_medium=referral&utm_content=creditCopyText">Unsplash</a>'
  }
];
