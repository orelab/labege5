
/*********************************************************

  UNE VERSION QUI CRÉÉE DYNAMIQUEMENT LE CAROUSEL
  À PARTIR DE LA VARIABLE carousel CRÉÉE DANS LE
  FICHIER carousel.js.

  => COMMENCEZ À LIRE LE SCRIPT À 
     PARTIR DES LIGNES 107 à 116 !

*********************************************************/



/*

  PREMIERE PARTIE, DES FONCTIONS UTILITAIRES

   Ces fonctions font des actions diverses. Leur contenu n'est
   pas important. Ce qui est important, c'est que leur nom soit 
   explicite et corresponde à une action claire.

*/


const update = id => { // Cette fonction met à jour le HTML

  if(id<1 || id>carousel.length || id==activeSlide){
    console.log(`tentative d'afficher une Slide inexistante ou déjà affichée (${id}).`);
    return;
  }

  activeSlide = Number(id);

  const disapearAnimation = [
    { transform: 'rotate(0) scale(1)' },
    { transform: 'rotate(360deg) scale(0)', display:"none" }
  ];

  // Animations des slides

  document
    .querySelectorAll("#slides>*")
    .forEach(slide => {
      slide
        .animate(disapearAnimation, 500)
        .addEventListener("finish", () => {
          slide.style.display = "none";

          document
            .querySelector("#slide" + activeSlide)
            .style
            .display = "flex"
        })
    });

  // Boutons en gras

  document
    .querySelectorAll("#telecommande span")
    .forEach(e => e.style.fontWeight = "normal");

  document
    .querySelector("#remote-" + activeSlide)
    .style
    .fontWeight = "bold";
}


const createSlide = data => {
  document
    .getElementById("slides")
    .innerHTML += `
      <div id="slide${data.id}">
        <p>${data.text}</p>
        <img src="assets/${data.image}" alt="" />
      </div>
    `;
}


const addingRemoteControl = id => {
  document
    .getElementById("remote-n")
    .insertAdjacentHTML(
      'beforebegin',
      `<span id="remote-${id}" data-id="${id}">${id}</span>`
    );

  document
    .getElementById("remote-" + id)
    .addEventListener("click", ev => {
      const id = ev.target.getAttribute("data-id");
      update(id);
    });
}


const addingFooterCredit = credit => {
  document
    .getElementById("credits")
    .innerHTML += `<li>${credit}</li>`;
}



/*

  SECONDE PARTIE, LE COEUR DU PROGRAMME EST VRAIMENT À CET ENDROIT

*/

// Cette variable définit quelle slide est visible (les autres devant 
// toutes être cachées). Pour la modifier, on fait appel à la fonction
// update(), qui met à jour la page web en même temps !

var activeSlide;



// La fonction buildSlide() est le début du script ! Elle est appliquée 
// sur chaque valeur de la variable carousel (une variable créée dans le 
// fichier carousel.js), et qui contient une liste d'objets. Chacun de 
// ces objets rassemble toutes les infos des slides.

const buildSlide = data => {

  createSlide(data);
  addingRemoteControl(data.id);
  addingFooterCredit(data.credit);
}

carousel.forEach(buildSlide); // c'est ici que tout commence !

update(carousel[0].id); // On active le premier slide




// Un événement pour chacun des boutons spécifiques "PREV" et "NEXT".

document
  .getElementById("remote-p")
  .addEventListener("click", ev => {
    update(activeSlide-1);
  });

document
  .getElementById("remote-n")
  .addEventListener("click", ev => {
    update(activeSlide+1);
  });