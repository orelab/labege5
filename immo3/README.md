# Projet Immo3


Un projet complet utilisant Symfony en back, et React en front

## Lancer Symfony

- cd back-symfony
- cp .env.sample .env
- # éditer le fichier .env, et remplacer les xxxx
- # lancer les migrations
- symfony server:start

## Lancer le front

- cd ../front-react
- npm install
- npm run dev

## Éléments à regarder den détail

- front : pagination