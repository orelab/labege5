import { useState, useEffect } from 'react'
import 'bootstrap/dist/css/bootstrap.css';

import Header from './Header'
import Intro from './Intro'
import Bien from './Bien'
import Pagination from './Pagination'
import config from '../config.json'


type Bien = {
  id: number
  nom: string
  description: string
  price: number
  image: string
}


function App() {


  const [biens, setBiens] = useState<Bien[]>([])
  const [page, setPage] = useState<string>("/api/biens?page=1")
  const [pagination, setpagination] = useState<Bien[]>([])


  useEffect(() => {

    fetch(`${config.api}${page}`)
      .then(resp => resp.json())
      .then(function (data) {

        setBiens(data["hydra:member"]);
        setpagination(data["hydra:view"]);
      });

  }, [page])


  return (
    <main role="main">
      <Header />
      <Intro />

      <div className="album py-5 bg-light">
        <div className="container">
          <div className="row">

            {biens.map( (b, id) =>
              <Bien key={id} data={b} />
            )}

          </div>
        </div>
      </div>

      <Pagination data={pagination} update={setPage} />

    </main>
  )
}

export default App
