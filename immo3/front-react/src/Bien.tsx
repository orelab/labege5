import config from '../config.json'
import './Bien.css'

type propsBien= {
  data: {
    id: number
    nom: string
    description: string
    price: number
    image: string
  }
}


export default function Bien(props:propsBien) {
  
  const styleCss = () => {
    return props.data.image
    ? {backgroundImage:`url('${config.api}/img/${props.data.image}')`}
    : {}
  }

  return (
    <div className="col-md-4">
      <div className="card mb-4 shadow-sm" 
        style={styleCss()}>
        <h5>{props.data.nom}</h5>
        <p>{props.data.description}</p>
        <div className="card-body">
          <div className="d-flex justify-content-between align-items-center">
            <small className="text-muted">{props.data.price}€</small>
          </div>
        </div>
      </div>
    </div>
  )
}