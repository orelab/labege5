import './Pagination.css'


type paginationProps = {
  data: {
    "hydra:first": string
    "hydra:previous"?: string
    "hydra:next"?: string
    "hydra:last": string
  }
  update: (e:string) => {}
}


export default function Pagination(props:paginationProps){

  const doLoad = (urlApi:string) => {
    props.update(urlApi);
  }


  return (
    <ul>

      {props.data["hydra:previous"]
        ? <li onClick={() => doLoad(props.data["hydra:first"])}>&lt;&nbsp; premier</li>
        : <li className="off">&lt;&nbsp; premier</li>
      }

      {props.data["hydra:previous"]
        ? <li onClick={() => doLoad(props.data["hydra:previous"])}>précédent</li>
        : <li className="off">précédent</li>
      }

      {props.data["hydra:next"]
        ? <li onClick={() => doLoad(props.data["hydra:next"])}>suivant</li>
        : <li className="off">suivant</li>
      }

      {props.data["hydra:next"]
        ? <li onClick={() => doLoad(props.data["hydra:last"])}>dernier &nbsp;&gt;</li>
        : <li className="off">dernier &nbsp;&gt;</li>
      }

    </ul>
  )
}

