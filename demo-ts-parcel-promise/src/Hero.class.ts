
import { Person } from "./types";

export default class Hero {

	firstname: string = ""
	lastname: string = ""

	constructor( p:Person ){
		this.firstname = p.p;
		this.lastname = p.n;

		console.log(`A new star is born. Please welcome ${p.p} ! ! !`);

		return this;
	}


	show() {
		document.querySelector("main>.row")!.innerHTML +=
		` <article class="col-sm-3 col-12">
				<h4>${this.firstname}</h4>
				<p>${this.lastname}</p>
			</article>
		`;
	}


}
