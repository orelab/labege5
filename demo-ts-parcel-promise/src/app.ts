import { Person } from "./types";
import Hero from "./Hero.class";

console.log("coucou");

let annuaire:Person[] = [
  {p:"Osama", n:"Axxxxxx"},
  {p:"Mohammed", n:"Axxxxxx"},
  {p:"Anthony", n:"Bxxxxxx"},
  {p:"Narayan", n:"Bxxxxxx"},
  {p:"Thibaut", n:"Bxxxxxx"},
  {p:"Jean-Baptiste", n:"Cxxxxxx"},
  {p:"Steven", n:"Fxxxxxx"},
  {p:"Florent", n:"Jxxxxxx"},
  {p:"Erlan", n:"Lxxxxxx"},
  {p:"Robert Daniel", n:"Lxxxxxx"},
  {p:"Marie-Emmanuela", n:"Lxxxxxx"},
  {p:"Jonathan", n:"Mxxxxxx"},
  {p:"Ophélie", n:"Nxxxxxx"},
  {p:"Danny", n:"Pxxxxxx"},
  {p:"Sampa", n:"Rxxxxxx"},
];

let warriors:Hero[] = [];

annuaire.forEach( p => {
  let w = new Hero(p);
  w.show();
  warriors.push(w);
});



/*
 *
 *  DEMO PROMISES
 * 
 */


const doClick = () => {


  const p = new Promise((resolve, reject) => {
    setTimeout(() => {
      alert("elephant");
      resolve('done');
    }, 2000);
  })

  p.then( () => {
    console.log("jaguar");
  })

}

document.getElementById("go")?.addEventListener("click", doClick);