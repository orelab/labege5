# InstaMars : l'Instagram des photos de Mars !

L'objectif de ce projet est d'abord de découvrir et apprendre 
Svelte JS...

Construisez un clône de l'application Instagram, dont le contenu
est tiré de l'API de la NASA (photos de Mars, par exemple).

Préalablement, identifiez les composants de la page à construire,
un fichier maquette.odp est présent dans le dépôt, et a pour 
vocation à vous aider à visualiser de possibles composants.






# create-svelte

Everything you need to build a Svelte project, powered by [`create-svelte`](https://github.com/sveltejs/kit/tree/master/packages/create-svelte).

## Creating a project

If you're seeing this, you've probably already done this step. Congrats!

```bash
# create a new project in the current directory
npm create svelte@latest

# create a new project in my-app
npm create svelte@latest my-app
```

## Developing

Once you've created a project and installed dependencies with `npm install` (or `pnpm install` or `yarn`), start a development server:

```bash
npm run dev

# or start the server and open the app in a new browser tab
npm run dev -- --open
```

## Building

To create a production version of your app:

```bash
npm run build
```

You can preview the production build with `npm run preview`.

> To deploy your app, you may need to install an [adapter](https://kit.svelte.dev/docs/adapters) for your target environment.
