<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;
use App\Entity\Chrono;
use Doctrine\ORM\EntityManagerInterface;



class ChronoController extends AbstractController
{


    #[Route('/', name: 'app_chrono')]
    public function index(): Response
    {
        return $this->render('chrono/index.html.twig', [
            'controller_name' => 'coucou',
        ]);
    }




    #[Route('/chrono/save/{start}/{stop}', name: 'app_chrono_save')]
    public function save(EntityManagerInterface $entityManager, int $start, int $stop): JsonResponse
    {
        $dtstart = new \DateTime();
        $dtstart->setTimestamp($start/1000);

        $dtstop = new \DateTime();
        $dtstop->setTimestamp($stop/1000);

        $chrono = new chrono();
        $chrono->setStart($dtstart);
        $chrono->setStop($dtstop);
        $entityManager->persist($chrono);
        $entityManager->flush();

        return $this->json([
            'start' => $start,
            'stop' => $stop,
            'response' => 'ok'
        ]);
    }





    #[Route('/dashboard', name: 'app_chrono_dashboard')]
    public function dashboard(EntityManagerInterface $entityManager): Response
    {
        $chronos = $entityManager->getRepository(Chrono::class)->findAll();

        return $this->render('chrono/dashboard.html.twig', [
            'controller_name' => 'coucou',
            'chronos' => $chronos
        ]);
    }
}
