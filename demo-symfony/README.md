# Projet démo Synfony


L'objectif est de voir ou revoir les notions de Controller,
Entity, Repository, Migration et vues Twig de Symfony.

## installation 

- git clone ...
- cd projet/
- composer install
- symfony server:start --port=8001

## à regarder 

- public/app.js
- src/Controller/ChronoController.php
- templates/chrono/index.html.twig
- templates/chrono/dashboard.html.twig

## diagramme de séquence

![diagramme de séquence](sequence.png)
