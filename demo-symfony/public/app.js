


const btn = document.querySelector("#chrono")

const cpt = document.querySelector("#compteur")


let start_date
let stop_date
let compteur = 0

let interval


btn.addEventListener("click", doClick)


function doClick() {

  if (!interval) {

    start_date = new Date()
    btn.innerHTML = "stop"

    interval = setInterval(() => {
      compteur++
      console.log(compteur)
      cpt.innerHTML = compteur
    }, 1000)
  }


  else {
    console.log("on arrête la machine !")
    clearInterval(interval)
    btn.innerHTML = "start"
    stop_date = new Date()
    compteur = 0
    interval = null

    const url = `/chrono/save/${start_date.getTime()}/${stop_date.getTime()}`
    console.log(url)


    fetch(url)
      .then(function (response) {
        return response.json();
      })
      .then(function (data) {
        if ('response' in data && data['response'] === 'ok') {
          console.log('done !')
        } else {
          console.log('oups...')
        }
      });
  }
}



