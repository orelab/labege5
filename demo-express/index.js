const express = require('express')
const app = express()
const port = 3000

app.set('view engine', 'ejs')

const path = require('path')
app.use('/static', express.static(path.join(__dirname, 'public')))

const axios = require('axios');

const API_URL = 'https://api.nasa.gov/mars-photos/api/v1/rovers/curiosity/photos?sol=1000&api_key=sFe47NHrzMvrmtXQa2HUnS0vozeJcRthvM0QHg4V'



app.get('/', (req, res) => {
  axios
    .get(API_URL)
    .then((resp) => {
      res.render('home',{photos:resp.data.photos})
    })
})



app.get('/image/:id', (req, res) => {
  const id = req.params.id;

  axios
    .get(API_URL)
    .then((resp) => {

      resp.data.photos.forEach(p => {
        if(p.id == id){
          console.log(p)
          res.render('image',{photo:p})
          return
        }
      })

      res.render('notfound',{id:id})
    })

})



app.listen(port, () => {
  console.log(`Example app listening on port ${port}`)
})
