
# Backend MVC

## Partie 1 : Express JS

1. Install & découverte
2. routes
3. fichiers statiques
4. templating
5. BDD


### Entrainement

Créez avec Express JS une page de bienvenue composée simplement d'un titre et d'un lien permettant d'accéder à une seconde page vous présentant. Toutes les informations personnelles doivent être stockées initialement dans des variables, et les pages construites avec un moteur de template (PUG, EJS).
Vous utilisez Bootstrap pour styler vos pages.

### Projet

Construisez un site qui permet de visualiser quelques photos du rover Curiosity. Pour cela, vous allez utiliser l'API suivante :

https://api.nasa.gov/mars-photos/api/v1/rovers/curiosity/photos?sol=1000&api_key=DEMO_KEY

La page d'accueil "/" de votre site doit afficher la liste des images. Cette liste doit contenir l'ID de l'image, ainsi que la date de prise de vue (earth_date).

Une seconde page - nommée "/image" - permet d'afficher les détails de la photo cliquée. Notez que cette page doit recevoir un paramètre, pour savoir quelle image doit être affichée en détails ! Cette page doit permettre de voir la photo, mais on doit aussi avoir le nom de la caméra qui a pris la photo, ainsi que le nom du rover.

**Aide**

- [extension pour visualiser du JSON dans le navigateur](https://chrome.google.com/webstore/detail/jsonvue/chklaanhfefbnpoihckbnefhakgolnmc)
- [outil pour récupérer les données d'une API JSON](https://axios-http.com/fr/)
- [doc sur le moteur de template EJS](https://ejs.co/#docs)


## Partie 2 : Laravel / Symfony 