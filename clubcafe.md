# Éval Club Café


Créer un site de soumission d'événements pour un café associatif,
sur lequel les habitués du club peuvent réaliser trois actions :
- consulter la liste des événements proposés
- proposer un nouvel événement
- signifier sa participation à un événement donné


Pas de création de compte, ni de système de validation d'événement.
Pour le gérant du café, il s'agit uniquement de récupérer les
intentions de ces clients. Pour l'événement, il n'a donc besoin que
du nom, de la date, et d'une brève description. Concernant les 
enregistrements de participations, il est utile de demander le nom
du participant, un numéro de téléphone, et évidemment, l'événement 
qui concerne la participation !


# Conditions de l'évaluation

- durée : deux jours
- travail individuel


# Attendus

Le développeur utilise obligatoirement GIT pendant le codage. Ce même 
dépôt est utilisé pour fournir l'ensemble des éléments attendus :

- les codes sources
- un README détaillant la procédure pour installer le projet
  Pensez à fournir le schéma de base de données (ou des fichiers de migration) !
- le MCD
- le lien vers les maquettes réalisées avec Figma

