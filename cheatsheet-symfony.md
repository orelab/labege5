
# Cheat sheet Symfony


## Installation   

- symfony check:requirements
- symfony new clubcafe --webapp
- cd clubcafe/
- php bin/console about
- symfony composer req api


## Créer/configurer la base de données

- configurer .env
- php bin/console doctrine:database:create
- php bin/console make:entity
- php bin/console make:migration
- php bin/console doctrine:migrations:migrate


## Lancer le serveur

symfony server:start
