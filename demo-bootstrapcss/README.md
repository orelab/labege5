# démo Bootstrap CSS


## Intégrer Bootstrap

- utiliser un CDN
- ou télécharger les fichiers
- ou utiliser npm

Dans tous les cas, il faut à la fin une balise LINK et une balise SCRIPT
pour intégrer les fichiers **bootstrap.min.css** et **bootstrap.bundle.min.js**.


## Notions importantes

- Les container
- Les grid
- Les components
    * Carousel
    * Navbar
- customiser Bootstrap (SASS)


## Bonus

- Les autres components (Alert, Modal...)
- styler les formulaires
- styler les tableaux HTML
- Bootstrap icons


## Liens

- [maquette](https://www.figma.com/file/sXp9P6fjn6hwzdBewcrDNP/Untitled?node-id=0%3A1&t=j7G56R0X86lD6Y1O-0)
- [documentation](https://getbootstrap.com/docs/5.3/getting-started/introduction/)
- [compiler bootstrap](https://www.freecodecamp.org/news/how-to-customize-bootstrap-with-sass/)


## Compiler Bootstrap

- sudo npm install -g sass // https://sass-lang.com/install
- cd [le dossier du projet]
- npm init
- npm install bootstrap@5.3.0-alpha1 // https://getbootstrap.com/docs/5.3/getting-started/download/
- mkdir scss
- touch scss/custom.scss
- // éditer le fichier scss/custom.scss
- sass scss/custom.scss css/bootstrap-custom.css
- // ne pas oublier de préciser dans la page web l'adresse du nouveau fichier **bootstrap-custom.css** !