
# Les objets, avec des Pokemon

## Notions

- classe
- instance (objet)
- des valeurs (variables, propriétés)
- des actions (fonctions, méthodes)
- héritage

## Pour aller plus loin

- le constructeur
- private, public, protected, final, static
- this
- accesseurs (getter) et mutateurs (setter)
