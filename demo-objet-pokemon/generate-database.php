<?php

/*

  Un script autonome pour créer une table avec des Pokémons !

*/


class DBGenerator {

  private $url = "https://pokeapi.co/api/v2/pokemon/?offset=0&limit=5000";

  private $mysqli;

  private $pokemonList = []; // liste intermédiaire, récupérée depuis l'url principale

  private $pokemons = []; // Lisqte détaillées des Pokémons


  function __construct(){
    mysqli_report(MYSQLI_REPORT_ERROR | MYSQLI_REPORT_STRICT);
    $this->mysqli = new mysqli("database", "root", $_ENV['MYSQL_ROOT_PASSWORD'], "pokemon");
    
    if (mysqli_connect_errno()) {
      printf("Échec de la connexion : %s\n", mysqli_connect_error());
      exit();
    }
  }


  private function getAllPokemon(){
    $json_url = $this->url;
    $json = file_get_contents($json_url);
    $this->pokemonList = json_decode($json, TRUE)["results"];
  }



  private function getPokemonDetails( $data ){

    $detail_url = $data["url"];
    $json = file_get_contents($detail_url);
    $data = json_decode($json, TRUE);
    
    $infos = array(
      "nom" => $data["name"],
      "type" => $data["types"][0]["type"]["name"],
      "niveau" => rand(1, 8),
      "pv" => $data["base_experience"],
      "symbole" => $data["sprites"]["front_default"],
      "numero" => $data["id"]
    );

    //array_push($this->pokemons, $infos);
    $this->dbsave( $infos );
  }


  private function dbsave( $pokemon ){

    $stmt = $this->mysqli->prepare("
      INSERT INTO pokemon 
        (nom, type, niveau, pv, symbole, numapi) 
      VALUES
        (?, ?, ?, ?, ?, ?)
    ");
    $stmt->bind_param(
      "ssiiss",
      $pokemon["nom"],
      $pokemon["type"],
      $pokemon["niveau"],
      $pokemon["pv"],
      $pokemon["symbole"],
      $pokemon["numero"],
    );
    $stmt->execute();

    if( $stmt->affected_rows > 0 )
      echo "ajouté " . $pokemon["nom"] . "\r\n";
    else
      echo "erreur " . $pokemon["nom"] . "\r\n";
    
  }

  
  public function run(){

    $this->getAllPokemon();

    foreach( $this->pokemonList as $p){
      $this->getPokemonDetails( $p );
    }

  }


  public function debug(){
    echo count($this->pokemonList) + "\r\n";
    echo count($this->pokemons) + "\r\n";
  }
}


$g = new DBGenerator();
$g->run();
$g->debug();