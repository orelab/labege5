<?php





/*

PLATEAU // Le plateau pour UN joueur

pokemon actif (Pokemon)
banc (liste de maxi 5 Pokemon)
deck (60 cartes)
defausse (liste vide)
recompenses (liste de 6 cartes)


*/



class Plateau {

  public $pokemonActif;

  public $banc = [];

  public $deck = [];

  public $defausse = [];

  public $recompense = [];


  public function remplirDeCartesAleatoires(){

    for($i=0 ; $i<60 ; $i++){

      switch( rand(0,2) ){
        case 0: $carte = new Pokemon(); break;
        case 1: $carte = new Dresseur(); break;
        case 2: $carte = new Energie(); break;
      }
      array_push($this->deck, $carte);
    }
  }


  public function afficher(){
    echo '<div id="plateau">';

    for( $i=0 ; $i<count($this->deck) ; $i++){
      $this->deck[$i]->afficher();
    }
    echo '</div>';
  }

  
}