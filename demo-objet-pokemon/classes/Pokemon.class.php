<?php


/*

POKEMON

type
PV
nom
niveau (1, 2)
évolution
symbole extension
numéro de carte

*/


class Pokemon extends Carte {

  private $type = "feu";
  
  private $PV = 10;
  
  private $nom = "Pikachu";
  
  private $niveau = 0;
  
  private $evo = null;
  
  private $symbole;
  
  private $numero;

  private $force = 3;
  private $defense = 1;


  /*
    Si on ne précise pas d'infos en paramètres du constructeur,
    alors le constructeur créée un Pokémon aléatoire en appelant
    la méthode generateRandomPokemon() qui fait appel à une API.
  */
  function __construct( $infos = null ){

    if( is_null($infos) ){
      $infos = $this->generateRandomPokemon();
    }

    foreach( $infos as $key => $value){
      $this->{$key} = $value;
    }
  }


  public function afficher(){

    $isdead = $this->PV <= 0 ? "dead" : "alive";
    
    echo "
    <div class='Pokemon $isdead'>
      <h2>$this->nom</h2>
      <p>vies : $this->PV</p>
      <img src='$this->symbole' />
    </div>";
  }



  /*
      On appelle cette méthode avec un paramètre, qui est un OBJET POKEMON
      exemple d'utilisation :

      $pikachu->attaque( $salamech );

  */
  public function attaque( Pokemon $cible ){

    $coup = $this->force - $cible->defense;

    $cible->perdreDesVies( $coup );
  }






  public function perdreDesVies( $nbPoints ){
    $this->PV -= $nbPoints;

    if( $this->PV <= 0 ){
      echo "<p>Je suis mort !</p>";
      $this->PV = 0;
    }
  }





  private function generateRandomPokemon(){
    $json_url = "https://pokeapi.co/api/v2/pokemon/?offset=0&limit=5000";
    $json = file_get_contents($json_url);
    $data = json_decode($json, TRUE);

    $key = array_rand($data["results"]);
  
    $detail_url = $data["results"][$key]["url"];
    $json = file_get_contents($detail_url);
    $data = json_decode($json, TRUE);

    $infos = array(
      "nom" => $data["name"],
      "type" => $data["types"][0]["type"]["name"],
      "niveau" => rand(1, 8),
      "pv" => $data["base_experience"],
      "symbole" => $data["sprites"]["front_default"],
      "numero" => $data["id"]
    );

    return $infos;
  }


}

